package application.home.com.riceballroll.controller.game.gameobjects;

import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import application.home.com.riceballroll.controller.game.util.Point;

/**
 * Created by markj on 05-Sep-16.
 */
interface CollisionShape {

    boolean intersects(CollisionShape obj);
    boolean intersectRect(Rect rect);
    boolean intersectCircle(Circle circle);
    Rect getShape();
    void setOrigin(float x, float y);
    Point getOrigin();
    CollisionShape copy();
}
