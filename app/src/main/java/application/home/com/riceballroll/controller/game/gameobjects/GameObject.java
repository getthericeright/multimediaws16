package application.home.com.riceballroll.controller.game.gameobjects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import application.home.com.riceballroll.controller.game.util.Point;
import application.home.com.riceballroll.controller.game.util.Vec;

/**
 * Created by markj on 21-Aug-16.
 */
public abstract class GameObject {
    protected Point position;
    protected Vec velocity;
    private CollisionShape collisionShape;
    private boolean collision;

    public abstract void checkCollision(GameObject obj, float time_delta);
    public abstract void draw(Canvas canvas);
    public abstract void update(float time_delta);

    public Vec getVelocity(){ return this.velocity; }
    public void setVelocity(Vec velocity){ this.velocity = velocity;}

    public Point getPosition(){ return this.position; }
    public void setPosition(Point position){ this.position = position; }

    public boolean collision(){
        return this.collision;
    }
    public void setCollision(boolean collision){
        this.collision = collision;
    }

    public CollisionShape getCollisionShape() {
        return this.collisionShape;
    }
    public void setCollisionShape(CollisionShape shape){ this.collisionShape = shape; }

    // TODO: delete this code
    // testing purpose
    protected Bitmap collide;
    protected Bitmap nonCollide;
    public void setCollisionImage(Bitmap img){
        this.collide = img;
    }

}

