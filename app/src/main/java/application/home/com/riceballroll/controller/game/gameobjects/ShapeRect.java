package application.home.com.riceballroll.controller.game.gameobjects;

import android.graphics.Rect;

import application.home.com.riceballroll.controller.game.util.Point;
import application.home.com.riceballroll.controller.game.util.Vec;

/**
 * Created by markj on 05-Sep-16.
 */
public class ShapeRect implements CollisionShape{

    private Rect rect;

    public ShapeRect(float left, float top, float right, float bottom){
        this.rect = new Rect((int)left, (int)top, (int)right, (int)bottom);
    }
    public ShapeRect(Rect shape){
        this.rect = new Rect(shape);
    }

    @Override
    public boolean intersects(CollisionShape obj) {
        return obj.intersectRect(this.rect);
    }

    @Override
    public boolean intersectRect(Rect rect) {
        return this.rect.intersects(rect.left, rect.top, rect.right, rect.bottom);
    }

    @Override
    public boolean intersectCircle(Circle circle) {
        float testX = circle.getOrigin().getX();
        float testY = circle.getOrigin().getY();

        // over left side of rect
        if(circle.getOrigin().getX()<=this.rect.left){
            testX = this.rect.left;
        }
        // over right side of rect
        else if(circle.getOrigin().getX()>=this.rect.right){
            testX = this.rect.right;
        }
        // over top of rect
        if(circle.getOrigin().getY() <= this.rect.top){
            testY = this.rect.top;
        } else if(circle.getOrigin().getY() >= this.rect.bottom){
            testY = this.rect.bottom;
        }

        // calculate distance of circle´s origin and nearest point of rect
        Vec vector = Point.vector(circle.getOrigin(), new Point(testX, testY));
        float distance = vector.getLength();

        if((distance-circle.getRadius())<=0)
            return true;

        return false;
    }

    @Override
    public Rect getShape() {
        return this.rect;
    }

    @Override
    public void setOrigin(float x, float y) {
        this.rect.offsetTo((int)(x-(this.rect.width()/2)),(int)(y-(this.rect.height())));
    }

    @Override
    public Point getOrigin() {
        return new Point(this.rect.centerX(), this.rect.centerY());
    }

    @Override
    public CollisionShape copy() {
        return new ShapeRect(this.rect);
    }
}
