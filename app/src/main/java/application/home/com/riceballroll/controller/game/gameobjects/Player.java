package application.home.com.riceballroll.controller.game.gameobjects;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import application.home.com.riceballroll.controller.game.util.Point;
import application.home.com.riceballroll.controller.game.util.Vec;

/**
 * Created by markj on 21-Aug-16.
 */
public class Player extends GameObject{

    private Paint color;
    private Bitmap player;
    private final float speed = 75.f;

    public Player(int width, int height, int xPos, int yPos, Bitmap player){
        this.position = new Point(xPos, yPos);
        this.velocity = new Vec(0, 0);
        this.color = new Paint();
        this.color.setARGB(127, 0, 255, 0);

        this.player = player;
        this.setCollisionShape(new ShapeCircle(xPos, yPos, width/2));

        // testing
        this.nonCollide = player;
    }

    @Override
    public void update(float time_delta){
        float newXPos = this.position.getX() + Math.round(this.velocity.getX() * time_delta * speed);
        float newYPos = this.position.getY() + Math.round(this.velocity.getY() * time_delta * speed);
        setPosition(new Point(newXPos, newYPos));
        this.getCollisionShape().setOrigin(this.getPosition().getX(), this.position.getY());
    }

    @Override
    public void draw(Canvas canvas){
        canvas.drawBitmap(this.player, null, this.getCollisionShape().getShape(), null);
    }

    @Override
    public void checkCollision(GameObject obj, float time_delta) {

        // TODO: solve problem with xOffset shape is equal to yOffset shape, even though they are different objects!!
        // problem: already inside of wall, when collided! (solved)
        // problem: player cant collide with top/bottom of wall, it slides inside of it
        float newXPos = this.position.getX() + Math.round(this.velocity.getX() * time_delta * speed);
        float newYPos = this.position.getY() + Math.round(this.velocity.getY() * time_delta * speed);

        newXPos += time_delta*(this.getVelocity().getX()/Math.abs(this.getVelocity().getX()));
        newYPos += time_delta*(this.getVelocity().getY()/Math.abs(this.getVelocity().getY()));

        CollisionShape xOffset = this.getCollisionShape().copy();
        CollisionShape yOffset = this.getCollisionShape().copy();

        xOffset.setOrigin(newXPos, this.getCollisionShape().getOrigin().getY());
        yOffset.setOrigin(this.getCollisionShape().getOrigin().getX(), newYPos);

        // check if player is already inside of obstacle
        if(xOffset.intersects(obj.getCollisionShape()) || yOffset.intersects(obj.getCollisionShape())){

            CollisionShape playerTmpShape = this.getCollisionShape();
            int left_orig = playerTmpShape.getShape().left;
            int top_orig = playerTmpShape.getShape().top;
            int right_orig = playerTmpShape.getShape().right;
            int bot_orig = playerTmpShape.getShape().bottom;

            CollisionShape objShape = obj.getCollisionShape();
            int left_obj = objShape.getShape().left;
            int top_obj = objShape.getShape().top;
            int right_obj = objShape.getShape().right;
            int bot_obj = objShape.getShape().bottom;

            // player right side collide
            if (right_orig > left_obj && (bot_orig > top_obj && top_orig < bot_obj)) {
                this.getVelocity().setX(0.f);
            }
            // player left side collide
            if (left_orig < right_obj && (bot_orig > top_obj && top_orig < bot_obj)) {
                this.getVelocity().setX(0.f);
            }
            // player top side collide
            if (top_orig < bot_obj && (right_orig > left_obj && left_orig < right_obj)) {
                this.getVelocity().setY(0.f);
            }
            // player bot side collide
            if (bot_orig > top_obj && (right_orig > left_obj && left_orig < right_obj)) {
                this.getVelocity().setY(0.f);
            }

        } else {
            player = nonCollide;
        }

    }
}
