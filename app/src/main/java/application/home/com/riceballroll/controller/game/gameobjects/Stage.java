package application.home.com.riceballroll.controller.game.gameobjects;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import java.util.ArrayList;

import application.home.com.riceballroll.controller.game.gameobjects.Player;

/**
 * Created by markj on 30-Aug-16.
 */
public class Stage{

    private ArrayList<Wall> walls;
    public static float WIDTH, HEIGHT;
    private Paint color;

    public Stage(float width, float height){
        this.walls = new ArrayList<>();
        this.WIDTH = width;
        this.HEIGHT = height;
        this.color = new Paint();
        this.color.setColor(Color.argb(255, 139,69,19));
    }

    public void addWall(float x1, float x2, float y1, float y2){
        int newX1 = (int)(this.WIDTH*x1);
        int newX2 = (int)(this.HEIGHT*x2);
        int newY1 = (int)(this.WIDTH*y1);
        int newY2 = (int)(this.HEIGHT*y2);
        this.walls.add(new Wall(new Rect(newX1, newX2, newY1, newY2)));
    }

    public void checkCollision(GameObject obj, float time_delta){
        for(Wall wall: walls){
            wall.checkCollision(obj, time_delta);
            obj.checkCollision(wall, time_delta);
        }
    }

    public void update(float time_delta){
        for(Wall wall:walls){
            wall.update(time_delta);
        }
    }

    public void draw(Canvas canvas){
        for(Wall wall:this.walls){
            wall.draw(canvas);
        }
    }
}
