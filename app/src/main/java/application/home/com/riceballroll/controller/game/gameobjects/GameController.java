package application.home.com.riceballroll.controller.game.gameobjects;

import android.graphics.Canvas;
import android.view.MotionEvent;

import application.home.com.riceballroll.controller.game.util.Point;

/**
 * Created by markj on 23-Aug-16.
 */
public interface GameController {

    void updatePlayer(Player player);

    boolean touchInside(float x, float y);
    void deviceTouched(boolean touch);

    void action_down(MotionEvent event);
    void action_move(MotionEvent event);
    void action_up(MotionEvent event);

    void update(float time_delta);
    void draw(Canvas canvas);
    void setPosition(Point pos);

}
