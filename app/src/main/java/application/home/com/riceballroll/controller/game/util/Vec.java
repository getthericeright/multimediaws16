package application.home.com.riceballroll.controller.game.util;

/**
 * Created by markj on 21-Aug-16.
 */
public class Vec {
    private float x,y;

    public Vec(float x, float y){
        this.x = x;
        this.y = y;
    }

    public static Vec add(Vec vec1, Vec vec2){
        return new Vec(vec1.getX()+vec2.getX(), vec1.getY()+vec2.getY());
    }

    public float getX(){ return this.x; }
    public void setX(float x){ this.x = x; }
    public float getY(){ return this.y; }
    public void setY(float y){ this.y = y; }

    public float getLength(){
        return (float)Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2));
    }

    public Vec normalize(){
        float factor = 1.f/getLength();
        return new Vec(this.x*factor, this.y*factor);
    }

}
