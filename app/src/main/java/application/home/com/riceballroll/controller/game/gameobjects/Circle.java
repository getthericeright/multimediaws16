package application.home.com.riceballroll.controller.game.gameobjects;

import application.home.com.riceballroll.controller.game.util.Point;

/**
 * Created by markj on 05-Sep-16.
 */
public class Circle {

    private float r;
    private Point origin;

    public Circle(float xPos, float yPos, float radius){
        this.origin = new Point(xPos, yPos);
        this.r = radius;
    }

    public Point getOrigin() {
        return origin;
    }

    public void setOrigin(Point origin) {
        this.origin = origin;
    }

    public float getRadius() {
        return r;
    }

    public void setRadius(float r) {
        this.r = r;
    }
    //TODO: think of a way to collide circles with rectangles

}
