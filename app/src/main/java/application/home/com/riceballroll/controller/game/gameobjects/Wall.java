package application.home.com.riceballroll.controller.game.gameobjects;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import application.home.com.riceballroll.controller.game.util.Vec;

/**
 * Created by markj on 07-Sep-16.
 */
public class Wall extends GameObject{

    private final float speed = 0.f;

    // define drawable object
    // private Bitmap/Rect wall
    private Paint color;

    public Wall(Rect rect){
        this.setCollisionShape(new ShapeRect(rect));
        this.color = new Paint();
        this.color.setARGB(255, 139,69,19);
    }
    public Wall(float left, float top, float right, float bottom){
        this.setCollisionShape(new ShapeRect(left, top, right, bottom));
        this.color = new Paint();
        this.color.setARGB(255, 139,69,19);
    }

    @Override
    public void checkCollision(GameObject obj, float time_delta) {
        if(this.getCollisionShape().intersects(obj.getCollisionShape())){
            this.setCollision(true);
        } else {
            this.setCollision(false);
        }
    }

    @Override
    public void draw(Canvas canvas)
    {
        canvas.drawRect(this.getCollisionShape().getShape(), this.color);
    }

    @Override
    public void update(float time_delta) {
        if(this.collision()){
            this.color.setARGB(255, 255, 0, 0);
        } else {
            this.color.setARGB(255, 139,69,19);
        }
    }
}
