package application.home.com.riceballroll.controller.game;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import application.home.com.riceballroll.R;
import application.home.com.riceballroll.controller.game.gameobjects.GameController;
import application.home.com.riceballroll.controller.game.gameobjects.Joystick;
import application.home.com.riceballroll.controller.game.gameobjects.Player;
import application.home.com.riceballroll.controller.game.gameobjects.Stage;
import application.home.com.riceballroll.controller.game.util.StageLoader;

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback{

    public final static int WIDTH = 480;
    public final static int HEIGHT = 800;
    private final static String TAG = "RiceBallMessage";
    private GameThread gameThread;
    private GameController controller;
    private Stage stage;

    //gamelayout
    private Background background;
    private Player player;

    // scaling factors
    private float scaleFactorX, scaleFactorY;

    public GamePanel(Context context) {
        super(context);
        Log.i(TAG, "Game panel constructed.");

        // add callback
        getHolder().addCallback(this);

        //start thread
        this.gameThread = new GameThread(getHolder(), this);

        setFocusable(true);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.i(TAG, "Game panel created.");

        //initiate
        this.background = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.wood_bg));
        this.player = new Player((int)((float)WIDTH/5.f), (int)((float)WIDTH/5.f), 10, 10, BitmapFactory.decodeResource(getResources(), R.drawable.sumo));

        // testing
        this.player.setCollisionImage(BitmapFactory.decodeResource(getResources(), R.drawable.riceball_dying));

        this.controller = new Joystick(WIDTH, HEIGHT);

        //load stage
        try {
            Log.i(TAG,"parsing...");
            this.stage = StageLoader.loadStage(getResources().openRawResource(R.raw.test_stage), WIDTH, HEIGHT);
        } catch (XmlPullParserException e) {
            Log.i(TAG, "Parser ERROR.");
            e.printStackTrace();
            e.printStackTrace();
        } catch (IOException e) {
            Log.i(TAG, "Input ERROR.");
            e.printStackTrace();
        }

        gameThread.setRunning(true);
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //Log.i(TAG, "Surface touched.");

        // scale to proper position
        Matrix scaleMat = new Matrix();
        scaleMat.setScale(1.f/scaleFactorX, 1.f/scaleFactorY);
        event.transform(scaleMat);

        // detect finger movement
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                controller.action_down(event);
                break;
            case MotionEvent.ACTION_MOVE:
                controller.action_move(event);
                break;
            case MotionEvent.ACTION_UP:
                controller.action_up(event);
                break;
        }
        controller.updatePlayer(this.player);
        return true;
    }


    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        Log.i(TAG, "Game panel changed: "+i+", "+i1+", "+i2);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.i(TAG, "Game panel destroyed.");
        boolean retry = true;
        while(retry) {
            try {
                this.gameThread.setRunning(false);
                this.gameThread.join();
            } catch (InterruptedException e) {
                // if interrupted retry
            }
            retry = false;
        }

        Log.i(TAG, "Game stopped.");
    }

    @Override
    public void draw(Canvas canvas){
        super.draw(canvas);
        //draw layout
        this.scaleFactorX = getWidth() / (float)WIDTH;
        this.scaleFactorY = getHeight() / (float)HEIGHT;

        final int savedState = canvas.save();
        canvas.scale(scaleFactorX, scaleFactorY);

        this.background.draw(canvas);
        this.stage.draw(canvas);
        this.controller.draw(canvas);
        this.player.draw(canvas);

        canvas.restoreToCount(savedState);
    }

    public void update(float time_delta){
        //check collisions first
        this.stage.checkCollision(this.player, time_delta);
        //update layout
        this.background.update(time_delta);
        this.stage.update(time_delta);
        this.player.update(time_delta);
        this.controller.update(time_delta);
    }


}
