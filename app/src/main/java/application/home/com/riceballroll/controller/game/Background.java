package application.home.com.riceballroll.controller.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;

/**
 * Created by markj on 21-Aug-16.
 */
public class Background {

    private Bitmap image;

    public Background(Bitmap image){
        this.image = image;
    }

    public void draw(Canvas canvas){
        canvas.drawBitmap(image, 0, 0, null);
    }

    public void update(float time_delta){

    }
}
