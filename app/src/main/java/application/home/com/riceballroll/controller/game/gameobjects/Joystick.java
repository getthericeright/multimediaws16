package application.home.com.riceballroll.controller.game.gameobjects;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;

import application.home.com.riceballroll.controller.game.util.Point;
import application.home.com.riceballroll.controller.game.util.Vec;

/**
 * Created by markj on 23-Aug-16.
 */
public class Joystick implements GameController {

    private float deviceWidth, deviceHeight;

    //Proxy knob
    private Paint padColor;
    private Paint knobColor;
    private Rect pad;
    private Rect knob;
    private Point origin;

    private boolean deviceTouch = false;
    private Point position;

    private boolean touchedInside;
    private boolean padMovement;
    private Point startTouch;

    private Vec playerVelocity;

    public Joystick(float deviceWidth, float deviceHeight){
        this.deviceWidth = deviceWidth;
        this.deviceHeight = deviceHeight;

        float topLeftX = deviceWidth/10;
        float topLeftY = (deviceHeight/5)*4;
        float width = deviceHeight/6;
        float height = deviceHeight/6;

        this.pad = new Rect((int)topLeftX, (int)topLeftY, (int)(topLeftX+width), (int)(topLeftY+height));
        this.knob = new Rect((this.pad.centerX())-(this.pad.width()/5), (this.pad.centerY())-(this.pad.height()/5), (this.pad.centerX())+(this.pad.width()/5), (this.pad.centerY())+(this.pad.height()/5));
        this.origin = new Point(this.pad.centerX(), this.pad.centerY());
        this.padColor = new Paint();
        this.knobColor = new Paint();
        this.knobColor.setARGB(255, 192, 192, 192);
        this.padColor.setARGB(255, 96, 96, 96);
        this.position = new Point(0,0);

        // init
        this.playerVelocity = new Vec(0,0);
    }

    @Override
    public void updatePlayer(Player player) {
        //if(!this.padMovement)
            player.setVelocity(this.playerVelocity);
    }

    @Override
    public boolean touchInside(float x, float y) {
        if(this.pad.contains((int)x, (int)y)){
            return true;
        }
        return false;
    }

    @Override
    public void deviceTouched(boolean touch) {
        this.deviceTouch = touch;
    }

    @Override
    public void action_down(MotionEvent event) {
        if (this.touchInside(event.getX(), event.getY())) {
            this.touchedInside = true;
            this.startTouch = new Point(event.getX(), event.getY());
            this.setPosition(new Point(event.getX(), event.getY()));
            this.deviceTouched(true);
        } else {
            this.touchedInside = false;
        }
    }

    @Override
    public void action_move(MotionEvent event) {
        if (touchedInside) {
            Point endTouch = new Point(event.getX(), event.getY());
            this.setPosition(new Point(event.getX(), event.getY()));
            Vec vec = Point.vector(startTouch, endTouch);

            if (vec.getX() != 0.f && vec.getY() != 0.f) {
                this.playerVelocity = Point.vector(startTouch, endTouch).normalize();
                this.padMovement = true;
            }
        }
    }

    @Override
    public void action_up(MotionEvent event) {
        this.touchedInside = false;
        this.deviceTouched(false);
        if (padMovement) {
            this.playerVelocity = new Vec(0, 0);
            this.padMovement = false;
        }
    }

    @Override
    public void update(float time_delta) {
        if(this.pad.contains((int)position.getX(), (int)position.getY())){
            this.knob.set((int)(this.position.getX()-(this.knob.width()/2)), (int)(this.position.getY()-(this.knob.height()/2)), (int)(this.position.getX()+(this.knob.width()/2)), (int)(this.position.getY()+(this.knob.height()/2)));
            } else {
            calculateIntersection();
        }
        if(!deviceTouch){
            this.knob.set((int)(this.origin.getX()-(this.knob.width()/2)), (int)(this.origin.getY()-(this.knob.height()/2)), (int)(this.origin.getX()+(this.knob.width()/2)), (int)(this.origin.getY()+(this.knob.height()/2)));
        }
    }

    public void setPosition(Point pos){
        this.position = pos;
    }

    // source = wikipedia
    private void calculateIntersection(){
        float x2 = position.getX();
        float y2 = position.getY();

        calcTop(x2, y2);
        calcBot(x2, y2);
        calcLeft(x2, y2);
        calcRight(x2, y2);
    }

    private void calcRight(float x2, float y2) {
        float x3 = this.pad.right;
        float y3 = this.pad.top;
        float y4 = this.pad.bottom;

        if(x2>x3){
            // knob touches right wall
            float newRight = this.pad.right+(this.knob.width()/2);
            float newTop = y2-(this.knob.height()/2);
            float newBottom = y2+(this.knob.height()/2);
            float newLeft = this.pad.right-(this.knob.width()/2);
            if(y2<y3){
                newTop = this.pad.top+(this.knob.height()/2);
                newBottom = this.pad.top-(this.knob.height()/2);
            } else if(y2>y4){
                newTop = this.pad.bottom-(this.knob.height()/2);
                newBottom = this.pad.bottom+(this.knob.height()/2);
            }
            this.knob.set((int)newLeft, (int)newTop, (int)newRight, (int)newBottom);
        }
    }

    private void calcLeft(float x2, float y2) {
        float y3 = this.pad.bottom;
        float x4 = this.pad.left;
        float y4 = this.pad.top;


        if(x2<x4){
            // knob touches left wall
            float newRight = this.pad.left+(this.knob.width()/2);
            float newTop = y2-(this.knob.height()/2);
            float newBottom = y2+(this.knob.height()/2);
            float newLeft = this.pad.left-(this.knob.width()/2);
            if(y2<y4){
                newTop = this.pad.top-(this.knob.height()/2);
                newBottom = this.pad.top+(this.knob.height()/2);
            } else if(y2>y3){
                newTop = this.pad.bottom-(this.knob.height()/2);
                newBottom = this.pad.bottom+(this.knob.height()/2);
            }
            this.knob.set((int)newLeft, (int)newTop, (int)newRight, (int)newBottom);
        }
    }

    private void calcBot(float x2, float y2) {
        float x3 = this.pad.right;
        float x4 = this.pad.left;
        float y4 = this.pad.bottom;

        if(y2>y4) {
            // knob touches bottom wall
            float newRight = x2+(this.knob.width()/2);
            float newTop = this.pad.bottom-(this.knob.height()/2);
            float newBottom = this.pad.bottom+(this.knob.height()/2);
            float newLeft = x2-(this.knob.width()/2);
            if(x2>x3){
                newRight = this.pad.right+(this.knob.width()/2);
                newLeft = this.pad.right-(this.knob.width()/2);
            } else if(x2<x4){
                newRight = this.pad.left-(this.knob.width()/2);
                newLeft = this.pad.left+(this.knob.width()/2);
            }
            this.knob.set((int)newLeft, (int)newTop, (int)newRight, (int)newBottom);
        }
    }

    private void calcTop(float x2, float y2) {
        float x3 = this.pad.left;
        float y3 = this.pad.top;
        float x4 = this.pad.right;

        if(y2<y3) {
            // knob touches top wall
            float newRight = x2+(this.knob.width()/2);
            float newTop = this.pad.top+(this.knob.height()/2);
            float newBottom = this.pad.top-(this.knob.height()/2);
            float newLeft = x2-(this.knob.width()/2);
            if(x2>x4){
                newRight = this.pad.right+(this.knob.width()/2);
                newLeft = this.pad.right-(this.knob.width()/2);
            } else if(x2<x3){
                newRight = this.pad.left-(this.knob.width()/2);
                newLeft = this.pad.left+(this.knob.width()/2);
            }
            this.knob.set((int)newLeft, (int)newTop, (int)newRight, (int)newBottom);
        }
    }

    @Override
    public void draw(Canvas canvas) {
        //reposition and rescale controller
        canvas.drawRect(this.pad.left, this.pad.top, this.pad.right, this.pad.bottom, padColor);
        canvas.drawRect(this.knob.left, this.knob.top, this.knob.right, this.knob.bottom, knobColor);
    }

}
