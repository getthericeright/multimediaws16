package application.home.com.riceballroll.controller.game.util;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

import application.home.com.riceballroll.controller.game.gameobjects.Stage;

/**
 * Created by markj on 30-Aug-16.
 */
public class StageLoader{

    private final static String TAG = "RiceBallMessage";

    public static Stage loadStage(InputStream stream, float width, float height) throws XmlPullParserException, IOException{
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(false);
        //factory.setValidating(true);

        XmlPullParser parser = factory.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(stream, null);

        return createStage(parser, width, height);
    }

    private static Stage createStage(XmlPullParser parser, float width, float height) throws XmlPullParserException, IOException{
        Stage stage = new Stage(width, height);

        int event = parser.getEventType();
        while(event != XmlPullParser.END_DOCUMENT){
            String name = parser.getName();
            Log.i(TAG, "Empty? " + name);
            if(name != null && !name.isEmpty()) {
                float[] values = new float[4];
                for (int i = 0; i < parser.getAttributeCount(); i++) {
                    values[i] = Float.valueOf(parser.getAttributeValue(i));
                }
                stage.addWall(values[0], values[1], values[2], values[3]);
            }
            event = parser.next();
        }
        return stage;
    }

}
