package application.home.com.riceballroll.controller.game.gameobjects;

import android.graphics.Rect;

import application.home.com.riceballroll.controller.game.util.Point;
import application.home.com.riceballroll.controller.game.util.Vec;

/**
 * Created by markj on 05-Sep-16.
 */
public class ShapeCircle implements CollisionShape{

    private Circle circle;

    public ShapeCircle(float xPos, float yPos, float radius){
        this.circle = new Circle(xPos, yPos, radius);
    }

    @Override
    public boolean intersects(CollisionShape obj) {
        return obj.intersectCircle(this.circle);
    }

    @Override
    public boolean intersectRect(Rect rect) {
        float testX = this.circle.getOrigin().getX();
        float testY = this.circle.getOrigin().getY();

        // over left side of rect
        if(this.circle.getOrigin().getX()<=rect.left){
            testX = rect.left;
        }
        // over right side of rect
        else if(this.circle.getOrigin().getX()>=rect.right){
            testX = rect.right;
        }
        // over top of rect
        if(this.circle.getOrigin().getY() <= rect.top){
            testY = rect.top;
        } else if(this.circle.getOrigin().getY() >= rect.bottom){
            testY = rect.bottom;
        }

        // calculate distance of circle´s origin and nearest point of rect
        Vec vector = Point.vector(circle.getOrigin(), new Point(testX, testY));
        float distance = vector.getLength();

        if((distance-this.circle.getRadius())<=0)
            return true;

        return false;
    }

    @Override
    public boolean intersectCircle(Circle circle) {
        float distance = Point.vector(this.circle.getOrigin(), circle.getOrigin()).getLength();
        if((distance-this.circle.getRadius()-circle.getRadius())<=0)
            return true;

        return false;
    }

    @Override
    public Rect getShape() {
        float left = this.circle.getOrigin().getX()-this.circle.getRadius();
        float top = this.circle.getOrigin().getY()-this.circle.getRadius();
        float right = this.circle.getOrigin().getX()+this.circle.getRadius();
        float bottom = this.circle.getOrigin().getY()+this.circle.getRadius();
        return new Rect((int)left, (int)top, (int)right, (int)bottom);
    }

    @Override
    public void setOrigin(float x, float y) {
        this.circle.setOrigin(new Point(x, y));
    }

    @Override
    public Point getOrigin() {
        return this.circle.getOrigin();
    }

    @Override
    public CollisionShape copy() {
        return new ShapeCircle(this.circle.getOrigin().getX(), this.circle.getOrigin().getY(), this.circle.getRadius());
    }
}
