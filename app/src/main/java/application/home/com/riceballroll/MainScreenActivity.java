package application.home.com.riceballroll;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import application.home.com.riceballroll.controller.game.util.StageLoader;

public class MainScreenActivity extends Activity {

    private static final String TAG = "RiceBallMessage";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // turn off title
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // set to full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //load layout
        setContentView(R.layout.activity_main_screen);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void onPlayClicked(View view){
        // start game panel
        Intent game = new Intent(MainScreenActivity.this, GamePanelActivity.class);
        startActivity(game);
    }


}
