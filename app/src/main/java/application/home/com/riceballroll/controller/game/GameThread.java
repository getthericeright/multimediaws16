package application.home.com.riceballroll.controller.game;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * Created by markj on 16-Aug-16.
 */
public class GameThread extends Thread{

    private static final String TAG = "RiceBallMessage";
    private boolean running = true;
    private SurfaceHolder surfaceHolder;
    private GamePanel gamePanel;
    public static Canvas canvas;

    //testing
    private Paint textColor;

    public GameThread(SurfaceHolder surfaceHolder, GamePanel gamePanel){
        super();
        this.surfaceHolder = surfaceHolder;
        this.gamePanel = gamePanel;
        // showing the fps
        this.textColor = new Paint();
        this.textColor.setARGB(255, 255, 0, 0);
        textColor.setTextSize(50);
        textColor.setStyle(Paint.Style.FILL);
    }

    // styling
    private int frameCount = 30;
    private float rate;
    private float fps;

    private float roundTwoDigits(float value){
        return Math.round(value*100.f)/100.f;
    }

    @Override
    public void run() {
        Log.i(TAG, "Game started.");

        long lastTime = System.nanoTime();

        while(running){
            long currentTime = System.nanoTime();

            //calculating fps
            long delta = (currentTime-lastTime);
            lastTime = currentTime;
            float rate = ((float)delta)/1000000f;
            float fps = 1000000000f/((float)delta);
            // factor of change per frame
            float fps_factor = 1.0f/fps;

            // update and draw game here
            try {
                canvas = this.surfaceHolder.lockCanvas();
            } catch (Exception e){
                Log.e(TAG, "Locking exception.");
                running = false;
                e.printStackTrace();
            }
            if(canvas!=null) {
                synchronized (surfaceHolder) {
                    this.gamePanel.update(fps_factor);
                    this.gamePanel.draw(canvas);
                    //show fps
                    if(frameCount>30) {
                        this.rate = rate;
                        this.fps = fps;
                        frameCount = 0;
                    }
                    canvas.drawText("Frametime = " + roundTwoDigits(this.rate) + "ms - FPS = " + roundTwoDigits(this.fps) + "fps", 0, 50, textColor);
                }
                surfaceHolder.unlockCanvasAndPost(canvas);
            }

            frameCount++;
        }
    }

    public void setRunning(boolean run){
        this.running = run;
    }
}
