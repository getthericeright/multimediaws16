package application.home.com.riceballroll.controller.game.util;

/**
 * Created by markj on 21-Aug-16.
 */
public class Point {
    private float x, y;

    public Point(float x, float y){
        this.x = x;
        this.y = y;
    }

    public float getX(){ return this.x; }
    public float getY(){ return this.y; }
    public void setX(float x){ this.x = x; }
    public void setY(float y){ this.y = y; }

    public static Vec vector(Point start, Point end){
        return new Vec(end.getX()-start.getX(), end.getY()-start.getY());
    }
}
